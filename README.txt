This Git repository is a companion to the Drupal Module Development course
The idea is that you can, if necessary, checkout the code at various stages of completion
after having tried to build the section of code yourself.
 
These stages of development  are tagged and coincide with the lesson numbers.

Do not worry if you do not have Git installed on your machine or are unfamiliar with it
 - you can simply download the code using the download link on the left and refer to it as 
we go through the build.