<?php

/**
 * @file
 * Include file to implement forms etc in flag_application_complete project.
 */

/**
 * Implements hook_form().
 */

function flag_application_complete_form($form, $form_state) {
  $form = array();
  // N.B. calls to join method cannot be chained together - hence ; at end of each line
  $sql = db_select('flag_application_complete', 'fa')->extend('PagerDefault');
  $sql->join('flagging', 'f', 'f.flagging_id = fa.flagging_id');
  $sql->join('node', 'n', 'f.entity_id = n.nid');
  $sql->join('users', 'u', 'f.uid = u.uid');
  $sql
    ->fields('fa', array('status'))
    ->fields('f', array('entity_id', 'timestamp', 'flagging_id'))
    ->fields('n', array('title'))
    ->fields('u', array('name', 'uid'))
    ->orderBy('timestamp', 'DESC')
    ->limit(15);
  $results = $sql->execute();

  $header = array(
    'title' => t('Flagged Content'),
    'name' => t('User Name'),
    'entity_id' => t('Node Flagged'),
    'uid' => t('User ID'),
    'flagging_id' => t('Flag ID'),
    'status' => t('Status'),
  );

  $rows = array();
  
  foreach ($results as $result) {
  
  switch ($result->status) {
    case 0: 
    $status = "Pending";
    break;
    case 1: 
    $status = "Approved";
    break;
    case 2: 
    $status = "Denied";
    break;
    }
    
    $rows[] = array(
      'title' => l($result->title, 'node/' . $result->entity_id),
      'name' => l($result->name, 'user/' . $result->uid),
      'entity_id' => $result->entity_id,
      'uid' => $result->uid,
      'flagging_id' => $result->flagging_id,
      'status' => $result->status,
      //'#attributes' => array('class' => array('application_row')),
    );    

  }

  $form['flag_application_status']['approvedeny'] = array(
      '#type' => 'select',
      '#title' => 'Actions',
      '#options' => array(
         1 => t('Approve'),
         2 => t('Deny'),
        ),
  );  

 // here we use table_select element of form to build table of results

  $form['flag_application_table'] = array(
      '#type' => 'tableselect',
      '#header' => $header,
      '#options' => $rows,
      '#empty' => t('No applications found.'),
      //'#attributes' => array('class' => array('applications')),
    );

  $form['pager'] = array('#markup' => theme('pager'));

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
  );

  return $form;
}

/**
 * Implements hook_form_validate().
 */

function flag_application_complete_form_validate($form, $form_state) {
  //dsm($form);
  //dsm($form_state);
  if ($form_state['values']['approvedeny'] != 1 && $form_state['values']['approvedeny'] != 2) {
    form_set_error('approvedeny', 'You have entered an incorrect value for the Actions');
  }
}

/**
 * Implements hook_form_submit().
 */

function flag_application_complete_form_submit($form, $form_state) {
  //dsm($form);
  //dsm($form_state);
  $action = $form_state['values']['approvedeny'];
  $values = array();
  foreach ($form_state['values']['flag_application_table'] as $key => $value) {
    if (is_string($value) == TRUE) {
      $values[] = $form_state['complete form']['flag_application_table']['#options'][$value];
    }
  }
  flag_application_administration($action, $values);
}

function flag_application_administration($action, $values) {
  //dsm($action);
  //dsm($values);
  foreach ($values as $key => $value) {
    $num_updated = db_update('flag_application_complete')
      ->fields(array('status' => $action))
      ->condition('flagging_id', $value['flagging_id'])
      ->execute();
  }
}
